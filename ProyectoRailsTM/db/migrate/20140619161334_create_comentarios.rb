class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.string :contenido
      t.string :email_autor

      t.timestamps
    end
  end
end
